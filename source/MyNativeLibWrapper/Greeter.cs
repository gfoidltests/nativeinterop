﻿using MyNativeLibWrapper.Internal;

namespace MyNativeLibWrapper
{
	public class Greeter
	{
		public void SayHi()
		{
			NativeMethods.SayHi();
		}
	}
}