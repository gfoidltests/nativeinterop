﻿using System;
using System.Runtime.InteropServices;

#if NET45
using System.IO;
#endif

namespace MyNativeLibWrapper.Internal
{
	internal static class NativeMethods
	{
#if NET45
		static NativeMethods()
		{
			string path   = Environment.GetEnvironmentVariable("PATH") ?? string.Empty;
			string dllDir = Path.GetDirectoryName(typeof(NativeMethods).Assembly.Location);

			if (Environment.Is64BitProcess)
				dllDir = Path.Combine(dllDir, "runtimes", "win-x64", "native");
			else
				dllDir = Path.Combine(dllDir, "runtimes", "win-x86", "native");

			path += ";" + dllDir;

			Environment.SetEnvironmentVariable("PATH", path);
		}
#endif
		//---------------------------------------------------------------------
		public static void SayHi() => say_hi();
		//---------------------------------------------------------------------
		[DllImport("MyNativeLib", CallingConvention = CallingConvention.Cdecl)]
		private static extern void say_hi();
	}
}