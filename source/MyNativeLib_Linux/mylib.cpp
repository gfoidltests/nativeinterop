#include "mylib.h"
#include <iostream>
#include <string>

using namespace std;

#ifdef _WIN32
	#ifdef _WIN64
		string name = "win-x64";
	#else
		string name = "win-x86";
	#endif
#elif __linux__
	string name = "linux-x64";
#endif

int getBitness()
{
	return sizeof(void*) == 8 ? 64 : 32;
}

void say_hi()
{
	cout << endl;
	cout << "Hi from " << name << ". ";
	cout << "I have " << getBitness() << " bit.";
	cout << endl << endl;
}