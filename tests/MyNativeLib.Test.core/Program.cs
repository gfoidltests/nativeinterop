﻿using MyNativeLibWrapper;

namespace MyNativeLib.Test
{
	class Program
	{
		static void Main(string[] args)
		{
			var greeter = new Greeter();
			greeter.SayHi();
		}
	}
}